'use strict';

function exclude() {
	const varNames = [
		'twitterSaviorExclusions',
		'twitterSaviorHideImages',
		'twitterSaviorHideRetweets',
		'twitterSaviorHideQuoteTweets',
		'twitterSaviorHideInCaseYouMissedIt',
		'twitterSaviorHideYouMightLike',
		'twitterSaviorHideAdvertisements',
		'twitterSaviorHideTrends',
		'twitterSaviorHideWhoToFollow',
	];
 
	chrome.storage.sync.get(varNames, items => {
		let unexclude = true;
		if (items.twitterSaviorHideInCaseYouMissedIt) {
			const dismissible = document.querySelector(
				'.DismissibleModule[data-item-type="recap_entry"');
			if (dismissible) {
				const youMissedIt = document.querySelector(
					'.DismissibleModule .recap-header');
				const temp = 'IncaseyoumisseditSeelessoften';
				if (youMissedIt &&
					youMissedIt.textContent.replace(/\s/g, '') === temp)
				{
					excludeSource(dismissible, 'In case you missed it');
					unexclude = false;
				}
			}
		}

		if (unexclude) {
			const dismissible = document.querySelector(
				'.DismissibleModule[data-item-type="recap_entry"');
			if (dismissible) {
				const youMissedIt = document.querySelector(
					'.DismissibleModule .recap-header');
				const temp = 'IncaseyoumisseditSeelessoften';
				if (youMissedIt &&
					youMissedIt.textContent.replace(/\s/g, '') === temp)
				{
					unexcludeSource(dismissible);
				}
			}
		}

		unexclude = true;
		if (items.twitterSaviorHideYouMightLike) {
			const dismissible = document.querySelector(
				'.DismissibleModule[data-item-type="recap_entry"');
			if (dismissible) {
				const youMightLike = document.querySelector(
					'.DismissibleModule .recap-header');
				const temp = 'YoumightlikeSeelessoften';
				if (youMightLike &&
					youMightLike.textContent.replace(/\s/g, '') === temp)
				{
					excludeSource(dismissible, 'You might like');
					unexclude = false;
				}
			}
		}

		if (unexclude) {
			const dismissible = document.querySelector(
				'.DismissibleModule[data-item-type="recap_entry"');
			if (dismissible) {
				const youMightLike = document.querySelector(
					'.DismissibleModule .recap-header');
				const temp = 'YoumightlikeSeelessoften';
				if (youMightLike &&
					youMightLike.textContent.replace(/\s/g, '') === temp)
				{
					unexcludeSource(dismissible);
				}	
			}
		}

		if (items.twitterSaviorHideTrends) {
			excludeSource(document.querySelector('.Trends'), 'Trends');
		} else {
			unexcludeSource(document.querySelector('.Trends'));
		}

		unexclude = true;
		if (items.twitterSaviorHideWhoToFollow) {
			const dismissible = document.querySelector(
				'.DismissibleModule[data-item-type="who_to_follow_entry"]');
			if (dismissible) {
				excludeSource(dismissible, 'Who to follow');
			}

			const sidebar = document.querySelector(
				'.module.roaming-module.wtf-module > .flex-module');
			excludeSource(sidebar, 'Who to follow');
			unexclude = false;
		}

		if (unexclude) {
			const dismissible = document.querySelector(
				'.DismissibleModule[data-item-type="who_to_follow_entry"]');
			if (dismissible) {
				unexcludeSource(dismissible);
			}

			const sidebar = document.querySelector(
				'.module.roaming-module.wtf-module > .flex-module');
			unexcludeSource(sidebar);
		}

		let selector = ':not(.GalleryTweet) > .tweet:not(.permalink-tweet), ' +
			':not(#profile-hover-container) > .ProfileCard, ' +
			'.AdaptiveNewsLargeImageHeadline, ' +
			'.trend-item';

		// collect exclusions
		let exclusions = items.twitterSaviorExclusions;
		// make sure not to break old versions with string props
		if (typeof(exclusions) === 'string') {
			exclusions = JSON.parse(items.twitterSaviorExclusions);
		}

		// collect all visible tweets
		Array.from(document.querySelectorAll(selector)).forEach(source => {
			// if the source doesn't exist, abort. not sure why it wouldn't
			// but there were definitely errors
			if (!source) {
				return;
			}

			let text = source.textContent;
			// tweets contain tons of extra text e.g. Like and Moments
			if (source.classList.contains('tweet')) {
				// get true text of the tweet
				const container = source.querySelector(
					'.js-tweet-text-container');
				if (container) {
					text = container.textContent + ' ';

					// add full name
					const name = source.querySelector('.fullname');
					if (name) {
						text += name.textContent + ' ';
					}

					// add username
					const handle = source.querySelector('.username');
					if (handle) {
						text += handle.textContent + ' ';
					}

					// add a possible quoted tweet
					const quote = source.querySelector('.QuoteTweet');
					if (quote) {
						text += quote.textContent + ' ';
					}
				}

				const link = source.querySelector(
					'.card-type-summary_large_image');
				if (link) {
					const iframe = link.querySelector('iframe');
					if (iframe &&
						iframe.contentDocument &&
						iframe.contentDocument.querySelector('.SummaryCard-content'))
					{
						text += iframe
							.contentDocument
							.querySelector('.SummaryCard-content').textContent +
							' ';
					}
				}
			}

			const excluding = exclusions.filter(exclusion => {
				return text
					.toLowerCase()
					.replace(/[,.'"()[\]/\\?!\-]/gi, '')
					.indexOf(exclusion) !== -1;
			});

			let isSinglePhoto = false;
			let isDoublePhoto = false;
			let isTriplePhoto = false;
			let isQuadruplePhoto = false;
			let isLinkPhoto = false;
			if (items.twitterSaviorHideImages) {
				if (source.querySelector('.AdaptiveMedia-singlePhoto')) {
					isSinglePhoto = true;
				} else if (source.querySelector('.AdaptiveMedia-doublePhoto')) {
					isDoublePhoto = true;
				} else if (source.querySelector('.AdaptiveMedia-triplePhoto')) {
					isTriplePhoto = true;
				} else if (source.querySelector('.AdaptiveMedia-quadPhoto')) {
					isQuadruplePhoto = true;
				} else if (!source.querySelector('.AdaptiveMedia-video') &&
					source.querySelector('.card-type-summary_large_image'))
				{
					isLinkPhoto = true;
				}
			}

			let isRetweet = false;
			if (items.twitterSaviorHideRetweets) {
				if (source.querySelector('.js-retweet-text')) {
					isRetweet = true;
				}
			}

			let isQuoteTweet = false;
			if (items.twitterSaviorHideQuoteTweets) {
				if (source.querySelector('.QuoteTweet')) {
					isQuoteTweet = true;
				}
			}

			let isAdvertisement = false;
			if (items.twitterSaviorHideAdvertisements) {
				if (source.classList.contains('promoted-tweet')) {
					isAdvertisement = true;
				}
			}

			if (excluding.length) {
				excludeSource(source, excluding.join(', '), true);
			} else if (isRetweet) {
				excludeSource(source, 'a retweet');
			} else if (isAdvertisement) {
				excludeSource(source, 'an advertisement');
			} else {
				unexcludeSource(source);
			}

			selector = '.AdaptiveMediaOuterContainer';
			const mediaContainer = source.querySelector(selector);
			if (isSinglePhoto) {
				excludeSource(mediaContainer, 'an image');
			} else if (isDoublePhoto) {
				excludeSource(mediaContainer, 'two images');
			} else if (isTriplePhoto) {
				excludeSource(mediaContainer, 'three images');
			} else if (isQuadruplePhoto) {
				excludeSource(mediaContainer, 'four images');
			} else {
				unexcludeSource(mediaContainer);
			}

			const quoteTweet = source.querySelector('.QuoteTweet');
			if (isQuoteTweet) {
				excludeSource(quoteTweet, 'a quote tweet');
			} else {
				unexcludeSource(quoteTweet);
			}

			const iframe = source.querySelector(
				'.card-type-summary_large_image > iframe');
			if (isLinkPhoto) {
				if (iframe &&
					iframe.contentDocument &&
					iframe.contentDocument.querySelector('.SummaryCard-image'))
				{
					excludeSource(iframe.contentDocument.querySelector(
						'.SummaryCard-image'),
						'a link image.');
				}
			} else {
				if (iframe &&
					iframe.contentDocument &&
					iframe.contentDocument.querySelector('.SummaryCard-image'))
				{
					unexcludeSource(iframe.contentDocument.querySelector(
						'.SummaryCard-image'));
				}
			}
		});
	});
}

document.addEventListener('click', document.body, e => {
	if (e.target.classList.contains('new-tweets-bar') !== -1) {
		setTimeout(exclude, 250);
	}
});

exclude();

setInterval(exclude, 1000);

function excludeSource(source, message, hideMessage) {
	if (!source) {
		return;
	}

	const children = Array.from(source.children);
	if (children.filter(aa => aa.dataset.twitterSavior).length) {
		return;
	}

	source.style.height = '90px';
	Array.from(source.children)
		.filter(child => !child.dataset.twitterSavior)
		.forEach(child => {
			child.style.visibility = 'hidden';
		});

	const container = document.createElement('div');
	container.className = 'twitterSaviorContainer';
	container.dataset.twitterSavior = true;
	container.style.position = 'relative';
	source.insertBefore(container, source.childNodes[0]);

	const excludedWords = document.createElement('span');
	excludedWords.className = 'excludedWords';
	excludedWords.dataset.twitterSavior = true;
	excludedWords.textContent = 'Twitter Savior ';
	container.appendChild(excludedWords);

	const showWords = document.createElement('button');
	showWords.className = 'showWords';
	showWords.dataset.twitterSavior = true;
	showWords.style.backgroundColor = '#E7EAFE';
	showWords.style.border = '1px solid #ACE2F7';
	showWords.style.color = '#F85C7B';
	showWords.style.padding = '7px';
	showWords.style.webkitBorderRadius = '3.5px';
	showWords.style.mozBorderRadius = '3.5px';
	showWords.style.borderRadius = '3.5px';
	container.appendChild(showWords);

	if (hideMessage) {
		showWords.textContent = 'excluded...';

		let isShown = false;
		showWords.onclick = () => {
			if (isShown) {
				showWords.textContent = 'excluded...';
			} else {
				showWords.textContent = 'excluded ' + message + '.';
			}

			isShown = !isShown;

			return false;
		};
	} else {
		showWords.textContent = 'excluded ' + message + '.';
	}

	const button = document.createElement('button');
	button.className = 'showHide';
	button.dataset.twitterSavior = true;
	button.style.position = 'relative';
	button.style.border = '1px solid #ACE2F7';
	button.style.backgroundColor = '#E7EAFE';
	button.style.color = '#000';
	button.innerHTML = '<span class="changer" ' +
		'style="position: absolute;' +
		'left: 50%;' +
		'top: 50%;' +
		'-webkit-transform: translate(-50%, -50%);' +
		'-moz-transform: translate(-50%, -50%);' +
		'-o-transform: translate(-50%, -50%);' +
		'transform: translate(-50%, -50%)">+</span>';
	container.appendChild(button);

	const buttonStyle = {
		position: 'fixed',
		right: '1rem',
		top: '1rem',
		width: '1rem',
		height: '1rem',
		border: '1px solid black',
		borderRadius: '0.1rem',
		background: 'white',
		zIndex: Number.MAX_SAFE_INTEGER,
	};

	for (let name in buttonStyle) {
		button.style[name] = buttonStyle[name];
	}

	button.style.position = 'absolute';
	button.style.right = 0;
	button.style.top = 0;
	button.style.zIndex = 999;

	button.onclick = () => {
		toggleExclusionVisibility(button, source, excludedWords, showWords);
		return false;
	};
}

function toggleExclusionVisibility(button, source, excludedWords, showWords) {
	if (button.textContent === '+') {
		source.style.height = '';

		Array.from(source.children)
			.filter(child => {
				return !child.dataset.twitterSavior;
			}).forEach(child => {
				child.style.visibility = '';
			});

		button.querySelector('span').textContent = '-';

		excludedWords.style.display = 'none';
		showWords.style.display = 'none';
	} else {
		source.style.height = '90px';

		Array.from(source.children)
			.filter(child => {
				return !child.dataset.twitterSavior;
			}).forEach(child => {
				child.style.visibility = 'hidden';
			});

		button.querySelector('span').textContent = '+';

		excludedWords.style.display = '';
		showWords.style.display = '';
	}
}

function unexcludeSource(source) {
	if (!source) {
		return;
	}

	source.style.height = '';

	Array.from(source.children)
		.filter(aa => aa.dataset.twitterSavior)
		.forEach(child => {
			child.parentNode.removeChild(child);
		});

	Array.from(source.children).forEach(child => {
		child.style.visibility = '';
	});
}