'use strict';

const panel = document.querySelector('#omissions');

const varNames = [
	'twitterSaviorExclusions',
	'twitterSaviorHideImages',
	'twitterSaviorHideRetweets',
	'twitterSaviorHideQuoteTweets',
	'twitterSaviorHideInCaseYouMissedIt',
	'twitterSaviorHideYouMightLike',
	'twitterSaviorHideAdvertisements',
	'twitterSaviorHideTrends',
	'twitterSaviorHideWhoToFollow',
];

chrome.storage.sync.get(varNames, items => {
	let exclusions = items.twitterSaviorExclusions;
	if (typeof(exclusions) === 'string') {
		exclusions = JSON.parse(exclusions);
	}

	if (!exclusions) {
		chrome.storage.sync.set({
			twitterSaviorExclusions: [],
		});

		exclusions = [];
	}

	exclusions.forEach(exclusion => {
		const container = document.createElement('div');
		container.className = 'exclusionContainer';
		panel.appendChild(container);

		const entry = document.createElement('input');
		entry.className = 'exclusion';
		entry.value = exclusion;
		entry.onkeyup = collectExclusions;
		container.appendChild(entry);

		const deleter = document.createElement('button');
		deleter.className = 'deleter';
		deleter.innerHTML =
			'<span class="centerHorizontallyAndVertically">-</span>';
		container.appendChild(deleter);

		deleter.onclick = () => {
			container.parentNode.removeChild(container);
			collectExclusions();
		};
	});

	Object.keys(items).forEach(name => {
		if (name === 'twitterSaviorExclusions') {
			return;
		}

		const selectors = {
			'twitterSaviorHideImages': '#hideImages',
			'twitterSaviorHideRetweets': '#hideRetweets',
			'twitterSaviorHideQuoteTweets': '#hideQuoteTweets',
			'twitterSaviorHideInCaseYouMissedIt': '#hideInCaseYouMissedIt',
			'twitterSaviorHideYouMightLike': '#hideYouMightLike',
			'twitterSaviorHideAdvertisements': '#hideAdvertisements',
			'twitterSaviorHideTrends': '#hideTrends',
			'twitterSaviorHideWhoToFollow': '#hideWhoToFollow',
		};

		document.querySelector(selectors[name]).checked = items[name];
	});

	if (!document.querySelector('input.exclusion')) {
		const container = document.createElement('div');
		panel.appendChild(container);

		const entry = document.createElement('input');
		entry.className = 'exclusion';
		entry.onkeyup = collectExclusions;
		container.appendChild(entry);

		const deleter = document.createElement('button');
		deleter.className = 'deleter';
		deleter.innerHTML =
			'<span class="centerHorizontallyAndVertically">-</span>';
		container.appendChild(deleter);

		deleter.onclick = () => {
			container.parentNode.removeChild(container);
			collectExclusions();
		};
	}

	const adder = document.createElement('button');
	adder.id = 'adder';
	adder.innerHTML =
		'<span class="centerHorizontallyAndVertically">+</span>';
	panel.appendChild(adder);

	adder.onclick = () => {
		const container = document.createElement('div');
		container.className = 'exclusionContainer';
		panel.insertBefore(container, adder);

		const entry = document.createElement('input');
		entry.className = 'exclusion';
		container.appendChild(entry);

		entry.onkeyup = collectExclusions;

		const deleter = document.createElement('button');
		deleter.className = 'deleter';
		deleter.innerHTML =
			'<span class="centerHorizontallyAndVertically">-</span>';
		container.appendChild(deleter);

		deleter.onclick = () => {
			container.parentNode.removeChild(container);
			collectExclusions();
		};
	};

	setInterval(collect, 1000);
});

function collect() {
	const twitterSaviorExclusions = collectExclusions();
	const optimizations = collectOptimizations();

	const joined = { twitterSaviorExclusions, };

	for (let name in optimizations) {
		joined[name] = optimizations[name];
	}

	chrome.storage.sync.set(joined, () => {});
}

function collectExclusions() {
	const selector = 'input.exclusion';
	const arr = Array.from(document.querySelectorAll(selector))	
		.map(exclusion => exclusion.value)
		.filter(exclusionValue => exclusionValue);

	return arr;
}


function collectOptimizations() {
	const selectors = {
		'#hideImages': 'twitterSaviorHideImages',
		'#hideRetweets': 'twitterSaviorHideRetweets',
		'#hideQuoteTweets': 'twitterSaviorHideQuoteTweets',
		'#hideInCaseYouMissedIt': 'twitterSaviorHideInCaseYouMissedIt',
		'#hideYouMightLike': 'twitterSaviorHideYouMightLike',
		'#hideAdvertisements': 'twitterSaviorHideAdvertisements',
		'#hideTrends': 'twitterSaviorHideTrends',
		'#hideWhoToFollow': 'twitterSaviorHideWhoToFollow',
	};

	const retObj = {};

	Object.keys(selectors).forEach(selector => {
		retObj[selectors[selector]] = document.querySelector(selector).checked;
	});

	return retObj;
}